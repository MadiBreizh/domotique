# Domotique

This is a personnal project use to record sensor into my home and display informations.

## Getting Started

Coming soon.

### Prerequisites

GRADLE
JAVA 8
NPM

### Installing

Coming soon.

## Deployment

Coming soon.

## Built With

* [GRADLE](https://gradle.org/) - Build Tool.

## Contributing

Coming soon.

## Authors

* **Erwan ANGOT** - *Initial work* - [MadiBreizh](https://gitlab.com/MadiBreizh)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

Coming soon.