/* **********************************************************************
 * DeviceTest.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.entity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.angot.domotique.AbstractTest;

import lombok.AccessLevel;
import lombok.Getter;

@RunWith(MockitoJUnitRunner.class)
@Getter(value = AccessLevel.PRIVATE)
public class DeviceTest extends AbstractTest<Device> {

    @Mock
    private Zone mZone1;

    @Mock
    private Zone mZone2;

    @Before
    public void initialize() {
        this.setEntity(new Device());

        MockitoAnnotations.initMocks(this);
    }

    /** Test add {@link Zone} method. */
    @Test
    public void testAddZone() {
        Assert.assertNull(this.getEntity().getZone());
        Assert.assertNotEquals(this.getMZone1(), this.getMZone2());
        Assert.assertEquals(this.getEntity(), this.getEntity().setZone(this.getMZone1()));
        Assert.assertEquals(this.getMZone1(), this.getEntity().getZone());
        Assert.assertEquals(this.getEntity(), this.getEntity().setZone(this.getMZone2()));
        Assert.assertEquals(this.getMZone2(), this.getEntity().getZone());
    }
}
