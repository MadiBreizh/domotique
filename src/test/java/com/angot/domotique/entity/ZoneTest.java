/* **********************************************************************
 * ZoneTest.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.entity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.angot.domotique.AbstractTest;

import lombok.AccessLevel;
import lombok.Getter;

@RunWith(MockitoJUnitRunner.class)
@Getter(value = AccessLevel.PRIVATE)
public class ZoneTest extends AbstractTest<Zone> {

    @Mock
    private Device mDevice1;

    @Mock
    private Device mDevice2;

    @Before
    public void initialize() {
    	
        this.setEntity(new Zone());

        MockitoAnnotations.initMocks(this);

        for (Device mDevice : new Device[] {this.getMDevice1(), this.getMDevice2()}) {
            Mockito.when(this.getEntity().addDevice(mDevice)).thenCallRealMethod();
            Mockito.when(this.getEntity().removeDevice(mDevice)).thenCallRealMethod();
        }
    }

    /** Test add {@link Device} method. */
    @Test
    public void testAddDevice() {
        Assert.assertTrue(this.getEntity().getDevices().isEmpty());
        Assert.assertNotEquals(this.getMDevice1(), this.getMDevice2());
        Assert.assertEquals(this.getEntity(), this.getEntity().addDevice(this.getMDevice1()));
        Assert.assertEquals(this.getMDevice1(), this.getEntity().getDevices().get(0));
        Assert.assertEquals("Doesn't add duplicates", 1, this.getEntity().addDevice(this.getMDevice1()).getDevices().size());
        Assert.assertEquals(this.getMDevice2(), this.getEntity().addDevice(this.getMDevice2()).getDevices().get(1));
        Assert.assertEquals(2, this.getEntity().getDevices().size());
        Assert.assertEquals(this.getEntity(), this.getMDevice1().getZone());
        Assert.assertEquals(this.getEntity(), this.getMDevice2().getZone());
        Assert.assertEquals(this.getEntity(), this.getEntity().removeDevice(this.getMDevice1()));
        Assert.assertEquals(1, this.getEntity().getDevices().size());
        Assert.assertEquals(this.getMDevice2(), this.getEntity().getDevices().get(0));
        Assert.assertTrue(this.getEntity().removeDevice(this.getMDevice2()).getDevices().isEmpty());
    }
}
