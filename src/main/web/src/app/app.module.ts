/***********************************************************************
 * app.module.ts, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Angot Erwan <erwan@malangot.fr>
 * License     : all right reserved
 * Last update : Jan 12, 2019
 *
 ***********************************************************************/

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { MatToolbarModule,
        MatSidenavModule,
        MatIconModule,
        MatListModule } from '@angular/material';
import { RouterModule } from '@angular/router';

import { ROUTES } from './app.route';
import { AppComponent } from './app.component';
import { ZoneService } from './services/zone-service/zone.service';
import { MockZoneService } from './services/zone-service/mock-zone.service';
import { ComponentModule } from './components/component.module';

/**
 * PARAMETERS
 */

import { TemperatureService } from './services/parameters/temperature-service/temperature.service';
import { HumidityService } from './services/parameters/humidity-service/humidity.service';
import { EnergyService } from './services/parameters/energy-service/energy.service';
import { PowerService } from './services/parameters/power-service/power.service';

// Use to define backend mode API or Mock
const USE_API = false;

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES),
    ComponentModule,
    MatToolbarModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
  ],
  providers: [
    { provide: ZoneService, useClass: USE_API ? ZoneService : MockZoneService },
    { provide: TemperatureService, useClass: USE_API ? TemperatureService : TemperatureService },
    { provide: HumidityService, useClass: USE_API ? HumidityService : HumidityService },
    { provide: PowerService, useClass: USE_API ? PowerService : PowerService },
    { provide: EnergyService, useClass: USE_API ? EnergyService : EnergyService },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
