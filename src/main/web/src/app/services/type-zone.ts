/***********************************************************************
 * type-zone.ts, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Angot Erwan <erwan@malangot.fr>
 * License     : all right reserved
 * Last update : Jan 12, 2019
 *
  ***********************************************************************/

  //TODO: Create namespace

export enum ZoneType {
    virtual = 'VIRTUAL',
    real = 'REAL'
}
