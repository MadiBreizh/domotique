/***********************************************************************
 * parameters.service.ts, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Angot Erwan <erwan@malangot.fr>
 * License     : all right reserved
 * Last update : Jan 12, 2019
 *
  ***********************************************************************/

import { map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { DataReceiver } from 'src/app/services/data-receiver';
import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';

@Injectable({
  providedIn: 'root'
})
export abstract class ParametersService extends ApiService<DataReceiver.RawData[]> implements DataReceiver.Service {

  loadData(): Observable<DataReceiver.ChartsTemplate> {
    return this.get()
      .pipe(map(data => data.map(item => {
        return {'value': item.value, 'name': new Date(item.createdAt.substring(0, 23).concat('Z'))};
      })))
      .pipe(map(data => {
        return {name: this.getPrefix(), series: data};
      }));
  }
}
