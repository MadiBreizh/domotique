/***********************************************************************
 * energy.service.ts, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Angot Erwan <erwan@malangot.fr>
 * License     : all right reserved
 * Last update : Jan 12, 2019
 *
  ***********************************************************************/

import { DataReceiver } from 'src/app/services/data-receiver';
import { Injectable } from '@angular/core';
import { ParametersService } from '../parameters.service';

@Injectable({
  providedIn: 'root'
})
export class EnergyService extends ParametersService implements DataReceiver.Service {

  getPrefix(): string {
    return 'energy';
  }
}
