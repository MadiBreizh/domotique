/***********************************************************************
 * temperature.service.ts, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Angot Erwan <erwan@malangot.fr>
 * License     : all right reserved
 * Last update : Jan 12, 2019
 *
  ***********************************************************************/

import { Injectable } from '@angular/core';
import { DataReceiver } from '../../data-receiver';
import { ParametersService } from '../parameters.service';

@Injectable({
  providedIn: 'root'
})
export class TemperatureService extends ParametersService implements DataReceiver.Service {

  getPrefix(): string {
    return 'temperature';
  }
}
