/***********************************************************************
 * data-receiver.ts, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Angot Erwan <erwan@malangot.fr>
 * License     : all right reserved
 * Last update : Jan 12, 2019
 *
  ***********************************************************************/

import { Observable } from 'rxjs';

export namespace DataReceiver {

  export interface RawData {
    id: number;
    disableTag: boolean;
    createdAt: string;
    value: number;
  }

  export interface Service {
    /**
     * Loads data to fill reports.
     * @returns Observable<ChartsTemplate[]>
     */
    loadData(): Observable<ChartsTemplate>;
  }

  //TODO: Move to chart Template.
  export interface ChartsTemplate {

    name: string;

    series: {
      /** Value of point. */
      value: number;
      /** Value for X coordonate. (2016-09-19T21:32:05.473Z) */
      name: string | number | Date;
    }[];
}
}

