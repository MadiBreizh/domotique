/***********************************************************************
 * api.service.ts, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Angot Erwan <erwan@malangot.fr>
 * License     : all right reserved
 * Last update : Jan 12, 2019
 *
  ***********************************************************************/

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export abstract class ApiService<T> {
  // TODO: Need to be export to environnement.
  private rootUrl = 'http://192.168.1.100:8081/api/v1/';

  //
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private http: HttpClient) { }

  get(url: string = ''): Observable<T> {
    return this.http.get<T>(this.rootUrl + this.getPrefix() + url, this.httpOptions);
  }

  abstract getPrefix(): string;
}
