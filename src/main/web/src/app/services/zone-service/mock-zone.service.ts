import { MonInterface } from './../../shared/charts/heat-map/heat-map.component';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

// @Injectable({
//   providedIn: 'root'
// })
export class MockZoneService implements MonInterface {
  private zones: ZoneTemplate[] = [
    {
      'id': 1,
      'name': 'azaeazezaezabc',
      'type': 'REAL'
    },
    {
      'id': 2,
      'name': 'dzaezaezaezaef',
      'type': 'REAL'
    },
    {
      'id': 3,
      'name': 'ghazezaezaezaeai',
      'type': 'REAL'
    },
    {
      'id': 4,
      'name': 'jkazezaezaezaezal',
      'type': 'VIRTUAL'
    }
  ];

  requestHeatmap() {
    console.log('depuis mon service');
  }

  constructor() { }

  public getAll(): Observable<ZoneTemplate[]> {
    return of(this.zones);
  }

  public deleteOne(id: number): Observable<ZoneTemplate[]> {
    this.zones.splice(id, 1);
    return this.getAll();
  }
}
