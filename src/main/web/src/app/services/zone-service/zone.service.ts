/***********************************************************************
 * zone.service.ts, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Angot Erwan <erwan@malangot.fr>
 * License     : all right reserved
 * Last update : Jan 12, 2019
 *
  ***********************************************************************/

import { ApiService } from './../api.service';
import { Observable } from 'rxjs';
import { MonInterface } from 'src/app/shared/charts/heat-map/heat-map.component';

// @Injectable({
//   providedIn: 'root'
// })
export class ZoneService extends ApiService<ZoneTemplate> implements MonInterface {

  getPrefix(): string {
    return 'zone';
  }

  getAll(): Observable<ZoneTemplate> {
    return this.get(this.getPrefix());
  }

  requestHeatmap() {
    console.log('toto');
    }

  public deleteOne(id: number): void {
    console.log('delete :', id);
  }
}
