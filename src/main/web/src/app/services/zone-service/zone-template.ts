interface ZoneTemplate {
  id: number;
  name: string;
  type: string;
}
