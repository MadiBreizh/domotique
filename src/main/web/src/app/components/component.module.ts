/***********************************************************************
 * component.module.ts, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Angot Erwan <erwan@malangot.fr>
 * License     : all right reserved
 * Last update : Jan 12, 2019
 *
  ***********************************************************************/

import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule,
        MatButtonModule,
        MatIconModule,
        MatInputModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSelectModule } from '@angular/material';
import { RouterModule } from '@angular/router';

import { ZoneComponent } from './zone/zone.component';
import { HomeComponent } from './home/home.component';
import { DeviceComponent } from './device/device.component';
import { ZoneFormComponent } from './zone/zone-form/zone-form.component';
import { ChartsModule } from '../shared/charts/charts.module';
import { ROUTES } from '../app.route';

@NgModule({
  imports: [
    RouterModule.forRoot(ROUTES),
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule
  ],
  providers: [
  ],
  exports: [
  ],
  declarations: [
    ZoneComponent,
    HomeComponent,
    DeviceComponent,
    ZoneFormComponent
  ]
})
export class ComponentModule {}
