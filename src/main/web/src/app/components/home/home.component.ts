/***********************************************************************
 * home.component.ts, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Angot Erwan <erwan@malangot.fr>
 * License     : all right reserved
 * Last update : Jan 12, 2019
 *
 ***********************************************************************/

import { ZoneService } from './../../services/zone-service/zone.service';
import { Component, OnInit } from '@angular/core';

import { HumidityService } from './../../services/parameters/humidity-service/humidity.service';
import { TemperatureService } from 'src/app/services/parameters/temperature-service/temperature.service';
import { PowerService } from 'src/app/services/parameters/power-service/power.service';
import { EnergyService } from './../../services/parameters/energy-service/energy.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(public service: ZoneService,
    public temperatureService: TemperatureService,
    public humidityService: HumidityService,
    public powerService: PowerService,
    public energyService: EnergyService) {  }

    ngOnInit() {
  }
}
