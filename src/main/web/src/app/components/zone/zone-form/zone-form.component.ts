/***********************************************************************
 * zone-form.component.ts, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Angot Erwan <erwan@malangot.fr>
 * License     : all right reserved
 * Last update : Jan 12, 2019
 *
  ***********************************************************************/

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ZoneType } from 'src/app/services/type-zone';

@Component({
  selector: 'app-zone-form',
  templateUrl: './zone-form.component.html',
  styleUrls: ['./zone-form.component.scss']
})
export class ZoneFormComponent implements OnInit {
  form: FormGroup;
  zones = ZoneType;

  constructor(private fb: FormBuilder, private route: ActivatedRoute) {
    this.form = this.fb.group({
      name: fb.control('', [Validators.required, Validators.minLength(3)]),
      type: fb.control('', [Validators.required])
    });
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('zoneId');

      if (id) {
        console.log('edit mode : ' + id);
      } else {
        console.log('create mode');
      }
    }

    onSubmit() {
      console.log(this.form.value);
    }
}
