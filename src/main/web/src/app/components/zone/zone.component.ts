/***********************************************************************
 * zone.component.ts, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Angot Erwan <erwan@malangot.fr>
 * License     : all right reserved
 * Last update : Jan 12, 2019
 *
  ***********************************************************************/

import { Component, OnInit } from '@angular/core';
import { ZoneService } from '../../services/zone-service/zone.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-zone',
  templateUrl: './zone.component.html',
  styleUrls: ['./zone.component.scss']
})
export class ZoneComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name', 'type', 'edit', 'delete'];
  zoneList: Observable<ZoneTemplate>;

  constructor(private zoneService: ZoneService) {}

  ngOnInit() {
    this.zoneList = this.zoneService.getAll();
  }

  onRemove(id: number) {
    this.zoneService.deleteOne(id);
  }

  onEdit(zone: ZoneTemplate) {
    console.log('edit :', zone);
  }

}
