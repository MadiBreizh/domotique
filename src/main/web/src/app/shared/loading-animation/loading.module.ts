/***********************************************************************
 * loading.module.ts, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Angot Erwan <erwan@malangot.fr>
 * License     : all right reserved
 * Last update : Jan 12, 2019
 *
  ***********************************************************************/

import { LoadingAnimationComponent } from './loading-animation.component';
import { NgModule } from '@angular/core';
import { MatProgressSpinnerModule } from '@angular/material';

@NgModule({
  imports: [
    MatProgressSpinnerModule
  ],
  providers: [
  ],
  exports: [
    LoadingAnimationComponent
  ],
  declarations: [
    LoadingAnimationComponent
  ]
})
export class LoadingModule {}
