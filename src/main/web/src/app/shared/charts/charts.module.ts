/***********************************************************************
 * charts.module.ts, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Angot Erwan <erwan@malangot.fr>
 * License     : all right reserved
 * Last update : Jan 12, 2019
 *
  ***********************************************************************/

import { NgModule } from '@angular/core';

import { NgxChartsModule } from '@swimlane/ngx-charts';

import { HeatMapComponent } from './heat-map/heat-map.component';
import { LoadingModule } from '../loading-animation/loading.module';
import { LineChartsComponent } from './line-charts/line-charts.component';



@NgModule({
  imports: [
    NgxChartsModule,
    LoadingModule
  ],
  providers: [
  ],
  exports: [
    HeatMapComponent,
    LineChartsComponent
  ],
  declarations: [
    HeatMapComponent,
    LineChartsComponent
  ]
})
export class ChartsModule {}
