/***********************************************************************
 * heat-map.component.ts, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Angot Erwan <erwan@malangot.fr>
 * License     : all right reserved
 * Last update : Jan 12, 2019
 *
  ***********************************************************************/

import { Component, OnInit, Input } from '@angular/core';

import { colorSets } from './../utils/color-sets';

const weekdayName = new Intl.DateTimeFormat('fr-fr', { weekday: 'short' });
const monthName = new Intl.DateTimeFormat('fr-fr', { month: 'short' });

export interface MonInterface {
  requestHeatmap();
}

export interface DataHeatMap {
  service: MonInterface;

}

export enum Periodicity {
  Weekly,


}


@Component({
  selector: 'app-heat-map',
  templateUrl: './heat-map.component.html',
  styleUrls: ['./heat-map.component.scss']
})
export class HeatMapComponent implements OnInit {
  public data: any[];
  // multi: any[];

  @Input() service: DataHeatMap;

  view: any[] = [700, 125];
  animations = true;
  fitContainer = false;
  colorScheme = {
    name: 'natural',
    selectable: true,
    group: 'Ordinal',
    domain: [
      '#bf9d76', '#e99450', '#d89f59', '#f2dfa7', '#a5d7c6', '#7794b1', '#afafaf', '#707160', '#ba9383', '#d9d5c3'
    ]
  };


  constructor() { }

  ngOnInit() {
    this.setColorScheme('cool');
    setTimeout(() => this.data = this.getCalendarData(), 1000);
    console.log(this.service);

    this.service.service.requestHeatmap();
  }

  setColorScheme(name) {
    this.colorScheme = colorSets.find(s => s.name === name);
  }

  getCalendarData(endDate = new Date()): any[] {

    const todaysDay = endDate.getDate();
    const thisDay = new Date(endDate.getFullYear(), endDate.getMonth(), todaysDay);

    // Monday
    const thisMonday = new Date(thisDay.getFullYear(), thisDay.getMonth(), todaysDay - thisDay.getDay() + 1);
    const thisMondayDay = thisMonday.getDate();
    const thisMondayYear = thisMonday.getFullYear();
    const thisMondayMonth = thisMonday.getMonth();

    // 52 weeks before monday
    const calendarData = [];
    const getDate = d => new Date(thisMondayYear, thisMondayMonth, d);

    for (let week = -52; week <= 0; week++) {
      const mondayDay = thisMondayDay + week * 7;
      const monday = getDate(mondayDay);

      // one week
      const series = [];
      for (let dayOfWeek = 7; dayOfWeek > 0; dayOfWeek--) {
        const date = getDate(mondayDay - 1 + dayOfWeek);

        // skip future dates
        if (date > endDate) {
          continue;
        }

        // value
        const value = dayOfWeek < 6 ? date.getMonth() + 1 : 0;

        series.push({
          date,
          name: weekdayName.format(date),
          value
        });
      }

      calendarData.push({
        name: monday.toString(),
        series
      });
    }

    return calendarData;
  }

  calendarAxisTickFormatting(mondayString: string) {
    const monday = new Date(mondayString);
    const month = monday.getMonth();
    const day = monday.getDate();
    const year = monday.getFullYear();
    const lastSunday = new Date(year, month, day - 1);
    const nextSunday = new Date(year, month, day + 6);
    return lastSunday.getMonth() !== nextSunday.getMonth() ? monthName.format(nextSunday) : '';
  }

  calendarTooltipText(c): string {
    return `
      <span class="tooltip-label">${c.label} • ${c.cell.date.toLocaleDateString()}</span>
      <span class="tooltip-val">${c.data.toLocaleString()}</span>
    `;
  }
}
