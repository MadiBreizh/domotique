/***********************************************************************
 * line-charts.component.ts, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Angot Erwan <erwan@malangot.fr>
 * License     : all right reserved
 * Last update : Jan 12, 2019
 *
  ***********************************************************************/

import { Component, OnInit, Input } from '@angular/core';
import { DataReceiver } from 'src/app/services/data-receiver';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-line-charts',
  templateUrl: './line-charts.component.html',
  styleUrls: ['./line-charts.component.scss']
})
export class LineChartsComponent implements OnInit {

  @Input('services') services: DataReceiver.Service[];

  data: DataReceiver.ChartsTemplate[];

  view: any[] = [1200, 800];
  colorScheme = {
    name: 'vivid',
    selectable: true,
    group: 'Ordinal',
    domain: [
      '#647c8a', '#3f51b5', '#2196f3', '#00b862', '#afdf0a', '#a7b61a', '#f3e562', '#ff9800', '#ff5722', '#ff4514'
    ]
  };

  ngOnInit() {
    forkJoin(this.services.map(service => service.loadData())).subscribe(data => this.data = data);
  }
}
