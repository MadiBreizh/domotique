/***********************************************************************
 * app.route.ts, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Angot Erwan <erwan@malangot.fr>
 * License     : all right reserved
 * Last update : Jan 12, 2019
 *
  ***********************************************************************/

import { Routes } from '@angular/router';

import { ZoneComponent } from './components/zone/zone.component';
import { ZoneFormComponent } from './components/zone/zone-form/zone-form.component';
import { HomeComponent } from './components/home/home.component';
import { DeviceComponent } from './components/device/device.component';

export const ROUTES: Routes = [
     { path: '', component: HomeComponent },
     { path: 'zone', component: ZoneComponent },
     { path: 'zone/:zoneId/edit', component: ZoneFormComponent },
     { path: 'zone/new', component: ZoneFormComponent },
     { path: 'device', component: DeviceComponent },
    ];
