/* **********************************************************************
 * DatabaseDateConverter.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 07, 2019
 *
 * ************************************************************************/

package com.angot.domotique.converter;

import java.text.MessageFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import lombok.extern.slf4j.Slf4j;

/** Convert ZonedDateTime saved into database. */
@Converter(autoApply = true)
@Slf4j
public class DatabaseDateConverter implements AttributeConverter<ZonedDateTime, String> {

    /** The conversion format. */
    public static final String FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZ";

    /** The conversion date formatter. */
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DatabaseDateConverter.FORMAT);

    /**
     * Convert given date to string for chosen format.
     * @param date ZonedDateTime
     * @return The formatted date
     */
    @Override
    public String convertToDatabaseColumn(ZonedDateTime attribute) {
        String result = null;

        if (attribute != null) {
            result = attribute.format(DatabaseDateConverter.DATE_FORMATTER);
        }

        return result;
    }

    @Override
    public ZonedDateTime convertToEntityAttribute(String dbData) {
        ZonedDateTime result = null;

        try {
            if (dbData != null && dbData.length() != 0) {
                result = ZonedDateTime.parse(dbData, DatabaseDateConverter.DATE_FORMATTER);
            }
        } catch (DateTimeParseException e) {
            DatabaseDateConverter.log.error(MessageFormat.format("Impossible to parse ''{0}'' to ZonedDateTime",
                    dbData));
        }

        return result;
    }

}
