/* **********************************************************************
 * IntervalTypeConverter.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Feb 01, 2019
 *
 * ************************************************************************/

package com.angot.domotique.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.web.bind.annotation.RequestParam;

import com.angot.domotique.entity.IntervalType;

/** Used to initialize {@link Enum} from {@link RequestParam} Controller. */
public class IntervalTypeConverter implements Converter<String, IntervalType> {

    @Override
    public IntervalType convert(String value) {
        try {
            return IntervalType.valueOf(value.toUpperCase());
        } catch (Exception e) {
            return null;
        }
    }
}
