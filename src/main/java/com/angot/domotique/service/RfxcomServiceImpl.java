package com.angot.domotique.service;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.angot.domotique.dto.RestDTO;
import com.angot.domotique.dto.receiver.ReceiverDTO;
import com.angot.domotique.dto.receiver.RfxcomDTO;
import com.angot.domotique.entity.Device;
import com.angot.domotique.exception.NotFoundException;
import com.angot.domotique.service.receiver.DataReceiverService;
import com.angot.domotique.service.receiver.RfxcomDetectorService;
import com.angot.domotique.service.receiver.RfxcomMeterService;
import com.angot.domotique.service.receiver.RfxcomSensorService;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.log4j.Log4j;

@Service
@Log4j
@Getter(value = AccessLevel.PRIVATE)
public class RfxcomServiceImpl implements RfxcomService {

    private static final String TYPE_SENSOR = "sensor";

    private static final String TYPE_METER = "meter";

    /** Declare to use to {@link Device} service. */
    @Autowired
    private DeviceService DeviceService;

    /** Declare to use to {@link RfxcomSensorService} service. */
    @Autowired
    @Qualifier("sensor")
    private DataReceiverService sensorService;

    /** Declare to use to {@link RfxcomMeterService} service. */
    @Autowired
    @Qualifier("meter")
    private DataReceiverService meterService;

    /** Declare to use to {@link RfxcomDetectorService} service. */
    @Autowired
    @Qualifier("detector")
    private DataReceiverService detectorService;

    @Override
    public void storeData(final ReceiverDTO receiverDTO) {
        RfxcomDTO dto = (RfxcomDTO) receiverDTO;

        final Device device = this.retrieveDevice(dto.getTopic());

        switch (dto.getType()) {
        case TYPE_SENSOR:
            this.getSensorService().storeData(dto, device);
            break;

        case TYPE_METER:
            this.getMeterService().storeData(dto, device);
            break;

        default:
            throw new NotYetImplementedException();
        }

        this.getDeviceService().updateDeviceInfo(device, dto.getStatus().getBattery(), dto.getStatus().getBattery());
    }

    /**
     * Retrieve {@link Device} from given i
     * @param dto
     * @return
     */
    private Device retrieveDevice(final String rawValue) {
        final String identifier = this.extractIdentifier(rawValue);

        final Device device = this.getDeviceService().getActiveDevice(identifier)
                .orElseThrow(() -> new NotFoundException());

        RfxcomServiceImpl.log.info("Found Device : " + device.getName());

        return device;
    }

    /**
     * Return {@link Optional} of {@link String} corresponding to an {@link Device#identifier} from given
     * {@link RestDTO}.
     * @param dto
     * @return
     */
    private String extractIdentifier(final String rawValue) {
        String result = null;

        Pattern patternId = Pattern.compile("[^/]+$");
        Matcher matcher = patternId.matcher(rawValue);

        if (matcher.find()) {
            result = matcher.group();
            RfxcomServiceImpl.log.info("Found identifier : " + result);
        } else {
            throw new NotFoundException();
        }

        return result;
    }
}
