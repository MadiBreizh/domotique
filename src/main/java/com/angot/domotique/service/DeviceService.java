/* **********************************************************************
 * DeviceService.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 14, 2019
 *
 * ************************************************************************/

package com.angot.domotique.service;

import java.time.ZonedDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.angot.domotique.entity.Device;
import com.angot.domotique.repository.DeviceRepository;

import lombok.AccessLevel;
import lombok.Getter;

@Service
@Getter(AccessLevel.PRIVATE)
public class DeviceService {

    @Autowired
    private DeviceRepository repository;

    /**
     * Return {@link Device} for given identifier
     * @param identifier
     * @return
     */
    @Cacheable("devices")
    public Optional<Device> getActiveDevice(final String identifier) {
        return this.getRepository().findByIdentifier(identifier);
    }

    /**
     * Updates info sync with {@link Device}.
     * @param device
     * @param battery
     * @param reception
     */
    @Transactional
    public void updateDeviceInfo(final Device device, final Long battery, final Long reception) {
        device.setBattery(battery);
        device.setReception(reception);
        device.setSync_date(ZonedDateTime.now());

        this.getRepository().save(device);
    }
}
