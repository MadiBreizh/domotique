/* **********************************************************************
 * ZoneService.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 14, 2019
 *
 * ************************************************************************/

package com.angot.domotique.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.angot.domotique.entity.Zone;
import com.angot.domotique.repository.ZoneRepository;

import lombok.AccessLevel;
import lombok.Getter;

@Getter(AccessLevel.PRIVATE)
@Service()
public class ZoneService {

    @Autowired
    private ZoneRepository repository;

    public List<Zone> getAll() {
        return this.getRepository().findAll();
    }
}
