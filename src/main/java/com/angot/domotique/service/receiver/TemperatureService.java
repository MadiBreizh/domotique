/* **********************************************************************
 * TemperatureService.java, epackhygiene Web
 *
 * Copyright 2018 Mickael Gaillard / TACTfactory
 * Description :
 * Author(s)   : Erwan Angot <erwan.angot@tactfactory.com>
 * License     : all right reserved
 * Last update : Jan 14, 2019
 *
 * ************************************************************************/

package com.angot.domotique.service.receiver;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.angot.domotique.entity.Device;
import com.angot.domotique.entity.parameter.Temperature;
import com.angot.domotique.repository.parameter.TemperatureRepository;

import lombok.AccessLevel;
import lombok.Getter;

@Service
@Getter(AccessLevel.PRIVATE)
public class TemperatureService {

    /** The {@link Temperature} repository. */
    @Autowired
    private TemperatureRepository repository;

    /**
     * Returns last values for each {@link Temperature} active.
     * @param device
     * @return
     */
    public Map<String, Long> getLastValue(final Device device) {
        
        return null;
    }
}
