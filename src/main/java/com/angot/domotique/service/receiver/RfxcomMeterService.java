/***********************************************************************
 * DataMeterService.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 08, 2019
 *
 * ************************************************************************/

package com.angot.domotique.service.receiver;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.angot.domotique.dto.receiver.ReceiverDTO;
import com.angot.domotique.entity.Device;
import com.angot.domotique.entity.parameter.Energy;
import com.angot.domotique.entity.parameter.Power;
import com.angot.domotique.repository.parameter.EnergyRepository;
import com.angot.domotique.repository.parameter.PowerRepository;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.log4j.Log4j;

@Service("meter")
@Log4j
@Getter(value = AccessLevel.PRIVATE)
public class RfxcomMeterService implements DataReceiverService {

    /** Declare to use to {@link Energy} repository. */
    @Autowired
    private EnergyRepository energyRepository;

    /** Declare to use to {@link Power} repository. */
    @Autowired
    private PowerRepository powerRepository;

    @Override
    public void storeData(final ReceiverDTO dto, Device device) {
        ReceiverDTO meterDTO = (ReceiverDTO) dto;

        if (meterDTO.getEnergy() != null) {
            // Round value 2 digit.
            BigDecimal roundValue = new BigDecimal(Double.toString(meterDTO.getEnergy().getValue()));
            roundValue = roundValue.setScale(2, RoundingMode.HALF_UP);

            Energy energy = new Energy(roundValue.doubleValue(), device.getZone(), device);
            this.getEnergyRepository().save(energy);

            RfxcomMeterService.log.info("ENERGY | Record data with value : " + energy.getValue());
       }

       if (meterDTO.getPower() != null) {
            Power power = new Power(meterDTO.getPower().getValue(), device.getZone(), device);
            this.getPowerRepository().save(power);

            RfxcomMeterService.log.info("POWER | Record data with value : " + power.getValue());
       }
    }

}
