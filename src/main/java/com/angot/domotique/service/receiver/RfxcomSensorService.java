/***********************************************************************
 * DataSensorService.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 08, 2019
 *
 * ************************************************************************/

package com.angot.domotique.service.receiver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.angot.domotique.dto.receiver.ReceiverDTO;
import com.angot.domotique.entity.Device;
import com.angot.domotique.entity.parameter.Humidity;
import com.angot.domotique.entity.parameter.Temperature;
import com.angot.domotique.repository.parameter.HumidityRepository;
import com.angot.domotique.repository.parameter.TemperatureRepository;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.log4j.Log4j;

@Log4j
@Service("sensor")
@Getter(value = AccessLevel.PRIVATE)
public class RfxcomSensorService implements DataReceiverService {

    /** Declare to use to {@link Temperature} repository. */
    @Autowired
    private TemperatureRepository temperatureRepository;

    /** Declare to use to {@link Humidity} repository. */
    @Autowired
    private HumidityRepository humidityRepository;

    @Override
    public void storeData(final ReceiverDTO dto, final Device device) {
        ReceiverDTO sensorDTO = (ReceiverDTO) dto;

        if (sensorDTO.getTemperature() != null) {
            Temperature temperature = new Temperature(sensorDTO.getTemperature().getValue(), device.getZone(), device);
            this.getTemperatureRepository().save(temperature);

            RfxcomSensorService.log.info("TEMPERATURE | Record data with value : " + temperature.getValue());
        }

        if (sensorDTO.getHumidity() != null) {
            Humidity humidity = new Humidity(sensorDTO.getHumidity().getValue(), device.getZone(), device);
            this.getHumidityRepository().save(humidity);

            RfxcomSensorService.log.info("HUMIDITY | Record data with value : " + humidity.getValue());
        }
    }
}
