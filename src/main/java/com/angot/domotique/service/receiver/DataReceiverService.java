/***********************************************************************
 * DataReceiverService.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 08, 2019
 *
 * ************************************************************************/

package com.angot.domotique.service.receiver;

import javax.transaction.Transactional;

import com.angot.domotique.dto.receiver.ReceiverDTO;
import com.angot.domotique.entity.Device;

@Transactional
public interface DataReceiverService {

    /**
     * Stores data linked with given {@link Device}.
     * @param dto
     * @param device
     */
    public void storeData(final ReceiverDTO dto, final Device device);
}
