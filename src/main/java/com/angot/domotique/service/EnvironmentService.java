/* **********************************************************************
 * EnvironmentService.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.AccessLevel;
import lombok.Getter;

@Service
@Getter(value = AccessLevel.PRIVATE)
public final class EnvironmentService {

    /** The application profile. */
    @Value("${spring.profiles.active:prod}")
    private String profile;

    /** The application profile. */
    @Value("${spring.profiles.fixtures:false}")
    private String fixtures;

    /** Returns {@code true} if current application profile is prod. */
    public boolean isProd() {
        return "prod".equals(this.getProfile());
    }

    /** Returns {@code true} if fixtures profile is enable. */
    public boolean isDev() {
        return "dev".equals(this.getProfile());
    }

    /** Returns {@code true} if current application profile is dev. */
    public boolean hasFixtures() {
        return "true".equals(this.getFixtures());
    }
}
