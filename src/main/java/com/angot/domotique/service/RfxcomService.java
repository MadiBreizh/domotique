/***********************************************************************
 * DeviceService.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 09, 2019
 *
 * ************************************************************************/

package com.angot.domotique.service;

import javax.transaction.Transactional;

import com.angot.domotique.dto.receiver.ReceiverDTO;
import com.angot.domotique.entity.Device;

@Transactional
public interface RfxcomService {

    /**
     * Store given information from {@link Device}.
     * @param dto
     */
    public void storeData(final ReceiverDTO dto);
}
