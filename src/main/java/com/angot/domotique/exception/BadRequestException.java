/* **********************************************************************
 * BadRequestException.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {

    /** Serial number. */
    private static final long serialVersionUID = -1222581598545550098L;

    /** Default constructor. */
    public BadRequestException() {
        super();
    }

    /**
     * Constructor with message.
     * @param message
     */
    public BadRequestException(String message) {
        super(message);
    }
}
