/* **********************************************************************
 * NoContentException.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NO_CONTENT)
public class NoContentException extends RuntimeException {

    /** Serial number. */
    private static final long serialVersionUID = -1096612318692571844L;

    /** Default constructor. */
    public NoContentException() {
        super("Entity was not found for given id.");
    }
}
