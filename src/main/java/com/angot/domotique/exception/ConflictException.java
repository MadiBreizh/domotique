/* **********************************************************************
 * ConflictException.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ConflictException extends RuntimeException{

	/** Serial number. */
	private static final long serialVersionUID = -6212390786748353145L;
	
	private static final String MSG_DEFAULT = "Entity already exist.";

	private static final String MSG_ENTITY = "Entity %s already exist.";


	/** Default constructor. */
	public ConflictException() {
		super(MSG_DEFAULT);
	}
	
	/**
	 * Constructor used to provide message with specified entity.
	 * @param entityName
	 */
	public ConflictException(final String entityName) {
		super(String.format(MSG_ENTITY, entityName));
	}
	
	
}
