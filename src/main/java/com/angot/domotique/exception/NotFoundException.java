package com.angot.domotique.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {

    /** Serial number. */
    private static final long serialVersionUID = 3251967338134330164L;

    private static final String MSG_DEFAULT = "Entity not found.";
    
    private static final String MSG_ID = "Entity not found for given id : %d";

    private static final String MSG_ENTITY_ID = "%1$s not found for given id : %2$d";

    
    /** Default constructor. */
    public NotFoundException() {
        super(MSG_DEFAULT);
    }
    
    /**
     * Constructor given message for Entity with specified id.
     * @param id of entity
     */
    public NotFoundException(final Long id) {
        super(String.format(MSG_ID, id));
    }
    
    /**
     * Constructor given message for named Entity with specified id.
     * @param id of entity
     */
    public NotFoundException(final String entityName, final Long id) {
        super(String.format(MSG_ENTITY_ID, entityName, id));
    }

    /**
     * Constructor given custom message.
     * @param message
     */
    public NotFoundException(final String message) {
        super(message);
    }

}
