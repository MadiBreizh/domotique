/* **********************************************************************
 * DomotiqueApplication.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique;

import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

import com.angot.domotique.entity.Device;
import com.angot.domotique.entity.Zone;
import com.angot.domotique.entity.ZoneType;
import com.angot.domotique.entity.parameter.Energy;
import com.angot.domotique.entity.parameter.Power;
import com.angot.domotique.entity.parameter.Temperature;
import com.angot.domotique.repository.DeviceRepository;
import com.angot.domotique.repository.ZoneRepository;
import com.angot.domotique.repository.parameter.EnergyRepository;
import com.angot.domotique.repository.parameter.PowerRepository;
import com.angot.domotique.repository.parameter.TemperatureRepository;
import com.angot.domotique.service.EnvironmentService;

import lombok.AccessLevel;
import lombok.Getter;

@SpringBootApplication
@Getter(value = AccessLevel.PRIVATE)
public class DomotiqueApplication extends SpringBootServletInitializer implements CommandLineRunner {

    @Autowired
    private EnvironmentService env;

    /** Repository for {@link Zone} entities. */
    @Autowired
    private ZoneRepository zoneRepository;

    /** Repository for {@link Device} entities. */
    @Autowired
    private DeviceRepository deviceRepository;

    /** Repository for {@link Energy} entities. */
    @Autowired
    private EnergyRepository energyRepository;

    /** Repository for {@link Power} entities. */
    @Autowired
    private PowerRepository powerRepository;


    /** Repository for {@link Power} entities. */
    @Autowired
    private TemperatureRepository temperatureRepository;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(DomotiqueApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(DomotiqueApplication.class, args);
    }

    @Transactional
    @Override
    public void run(String... args) throws Exception {
        if (this.getEnv().hasFixtures()) {
            // TODO: Add faker with parameter to set quantity in Resources.
            this.loadFixtures();
        }
    }

    private void loadFixtures() {
        /***************************** Device *********************/
        List<Device> devices = Arrays.asList(
                new Device("OWL", "0x8452"),
                new Device("OREGON", "0x6F01"));
        this.deviceRepository.save(devices);
        /***************************** Zone ***********************/
        List<Zone> zones = Arrays.asList(
                new Zone("Appartement", ZoneType.REAL).addDevice(devices.get(0)),
                new Zone("Chambre Anna", ZoneType.REAL).addDevice(devices.get(1)));
        this.zoneRepository.save(zones);
        /***************************** Meter **********************/

        /*** TEMPERATURE ***/
        this.getTemperatureRepository().save(
                new Temperature(15.0, this.getZoneRepository().getOne(1L), this.getDeviceRepository().getOne(1L))
                );

        /*** ENERGY ***/
        this.energyRepository.save(new Energy(5.0, zones.get(0), devices.get(0)));

        /*** POWER ***/
        this.powerRepository.save(new Power(50.0, zones.get(0), devices.get(0)));
        /***************************** Sensor *********************/
    }
}
