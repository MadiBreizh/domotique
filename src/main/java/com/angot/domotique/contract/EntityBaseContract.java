/* **********************************************************************
 * EntityBaseContract.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 07, 2019
 *
 * ************************************************************************/

package com.angot.domotique.contract;

import com.angot.domotique.entity.EntityBase;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/** Contracts about {@link EntityBase} class. */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EntityBaseContract {

    /** Column id. */
    public static final String COL_ID = "id";

    /** Column createdAt. */
    public static final String COL_CREATEDAT = "created_at";

    /** Column disable tag. */
    public static final String COL_DISABLE_TAG = "disable_tag";
}
