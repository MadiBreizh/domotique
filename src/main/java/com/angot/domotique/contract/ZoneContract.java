package com.angot.domotique.contract;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/** Contracts about {@link Zone} class. */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ZoneContract {

    /** Database table name. */
    public static final String TABLE_NAME = "sys_zone";
	
	/** Column zone id. */
    public static final String COL_ZONE_ID = "zone_id";
    
	/** Field name. */
    public static final String COL_NAME = "name";

	/** Field type. */
    public static final String COL_TYPE = "type";

}
