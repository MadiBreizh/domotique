/* **********************************************************************
 * PowerContract.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 07, 2019
 *
 * ************************************************************************/

package com.angot.domotique.contract.parameter;

import com.angot.domotique.entity.parameter.Power;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/** Contracts about {@link Power} class. */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PowerContract {

    /** Database table name. */
    public static final String TABLE_NAME = "app_param_power";

    /** Field value. */
    public static final String COL_VALUE = "value";  
}
