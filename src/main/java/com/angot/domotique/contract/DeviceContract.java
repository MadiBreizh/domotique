/* **********************************************************************
 * DeviceContract.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 07, 2019
 *
 * ************************************************************************/

package com.angot.domotique.contract;

import com.angot.domotique.entity.Device;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/** Contracts about {@link Device} class. */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DeviceContract {

    /** Database table name. */
    public static final String TABLE_NAME = "sys_device";
    
	/** Column zone id. */
    public static final String COL_DEVICE_ID = "device_id";
    
    /** Field name. */
    public static final String COL_NAME = "name";

    /** Field identifier. */
    public static final String COL_IDENTIFIER = "identifier";
    
    /** Field last sync date. */
    public static final String COL_SYNC_DATE = "sync_date";
    
    /** Field last sync date. */
    public static final String COL_LVL_BATTERY = "lvl_battery";

    /** Field last sync date. */
    public static final String COL_LVL_RECEPTION = "lvl_reception";

}
