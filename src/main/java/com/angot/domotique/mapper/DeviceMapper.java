package com.angot.domotique.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.angot.domotique.dto.DeviceDTO;
import com.angot.domotique.entity.Device;

/** Uses to map fields between entity {@link Device} and dto {@link DeviceDTO}. */
@Service
public class DeviceMapper extends AbstractMapper implements Mapper<DeviceDTO, Device> {

    @Override
        public DeviceDTO to(final Device entity) {
            return this.getModelMapper().map(entity, DeviceDTO.class);
    }

    @Override
    public Device from(final DeviceDTO dto, final Device entity) {
        if (dto.getName() != null) {
            entity.setName(dto.getName());
        }

        if (dto.getIdentifier() != null) {
            entity.setIdentifier(dto.getIdentifier());
        }

        return entity;
    }

    @Override
   public List<DeviceDTO> to(List<Device> entities) {
       return entities.stream().map(entity -> this.to(entity)).collect(Collectors.toList());
   }

    @Override
    public List<Device> from(List<DeviceDTO> dtos) {
        return null;
//        return dtos.stream().map(dto -> this.from(dto)).collect(Collectors.toList());
    }

}
