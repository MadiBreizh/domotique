/* **********************************************************************
 * LatenceMapper.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.angot.domotique.dto.parameter.LatenceDTO;
import com.angot.domotique.entity.Zone;
import com.angot.domotique.entity.parameter.Latence;
import com.angot.domotique.exception.NotFoundException;
import com.angot.domotique.repository.ZoneRepository;

import lombok.AccessLevel;
import lombok.Getter;

/** {@link Latence} Service. */
@Service
@Getter(value = AccessLevel.PRIVATE)
public class LatenceMapper extends AbstractMapper {

    @Autowired
    private ZoneRepository zoneRepository;

    /**
     *
     * @param dto
     * @return
     * @throws Exception
     */
    public Latence from(final LatenceDTO dto) {
        final Latence entity = new Latence();
        final Zone zone = this.getZoneRepository()
                    .findByName(dto.getHostname())
                        .map(e -> e)
                        .orElseThrow(() -> new NotFoundException(dto.getHostname() + " not tracked !"));

        entity.setValue(dto.getLatence()).setZone(zone);
        return entity;
    }

    /**
     *
     * @param entity
     * @return
     */
    public LatenceDTO to(final Latence entity) {
        LatenceDTO dto = new LatenceDTO();

        dto.setHostname(entity.getZone().getName())
                   .setLatence(entity.getValue())
                   .setCreatedAt(entity.getCreatedAt());

        return dto;
    }

    /**
     *
     * @param dtos
     * @return
     */
    public List<Latence> from(final List<LatenceDTO> dtos) {
        final List<Latence> entities = new ArrayList<>();

        for (LatenceDTO dto : dtos) {
            entities.add(this.from(dto));
        }

        return entities;
    }

    /**
     *
     * @param entities
     * @return
     */
    public List<LatenceDTO> to(final List<Latence> entities) {
        final List<LatenceDTO> dtos = new ArrayList<>();

        for (Latence entity : entities) {
            dtos.add(this.to(entity));
        }

        return dtos;
    }
}
