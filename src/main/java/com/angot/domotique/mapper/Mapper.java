package com.angot.domotique.mapper;

import java.util.List;

import com.angot.domotique.dto.RestDTO;
import com.angot.domotique.entity.EntityBase;

public interface Mapper<T extends RestDTO, R extends EntityBase> {

    /**
    * Convert given entity {@link R} to a DTO {@link T}.
    * @param <R> entity
    * @return
    */
    public T to(final R entity);

    /**
    * Convert given DTO {@link T} to an entity {@link R}.
    * @param dto
    * @return
    */
    public R from(final T dto, final R entity);

    /**
    * Convert List of entities {@link R} to a list of dtos {@link T}.
    * @param entities
    * @return
    */
    public List<T> to(List<R> entities);

    /**
    * Convert List of dtos {@link T} to a list of entities {@link R}.
    * @param entities
    * @return
    */
    public List<R> from(List<T> dtos);
}
