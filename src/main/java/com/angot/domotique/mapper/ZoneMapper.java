/* **********************************************************************
 * ZoneMapper.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.angot.domotique.dto.ZoneDTO;
import com.angot.domotique.entity.Zone;

/** Use to map fields between entity {@link Zone} and dto {@link ZoneDTO}. */
@Service
public class ZoneMapper extends AbstractMapper {

    /**
     * Convert given entity {@link Zone} in a DTO {@link ZoneDTO}.
     * @param entity
     * @return
     */
    public ZoneDTO to(final Zone entity) {
        return this.getModelMapper().map(entity, ZoneDTO.class);
    }

    /**
    * Convert given DTO {@link ZoneDTO} to an entity {@link Zone}.
    * @param entity
    * @return dto
    */
   public Zone from(final ZoneDTO dto, final Zone entity) {
       entity.setName(dto.getName());

       if (dto.getType() != null) {
           entity.setType(dto.getType());
       }

       return entity;
   }

    /**
     * Convert List of entities {@link Zone} to a list of dtos {@link ZoneDTO}
     * @param entities
     * @return
     */
    public List<ZoneDTO> to(List<Zone> entities) {
        return entities.parallelStream()
                .map(entity -> this.to(entity))
                .collect(Collectors.toList());
    }

}
