/* **********************************************************************
 * DeviceRepository.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.angot.domotique.entity.Device;

/** Repository use for {@link Device} entity. */
public interface DeviceRepository extends BaseRepository<Device> {

    /**
     * Returns active {@link Device} for given identifier.
     * @param identifier
     * @return
     */
    @Query(value = "SELECT d"
            + " FROM #{#entityName} d"
            + " WHERE d.disableTag = False"
                + " AND d.identifier = :identifier")
    Optional<Device> findByIdentifier(@Param("identifier") String identifier);

}
