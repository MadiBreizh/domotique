/* **********************************************************************
 * TrackRepository.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 07, 2019
 *
 * ************************************************************************/

package com.angot.domotique.repository.parameter;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import com.angot.domotique.entity.EntityTrack;
import com.angot.domotique.entity.IntervalType;
import com.angot.domotique.entity.parameter.NodeProjection;
import com.angot.domotique.repository.BaseRepository;

@NoRepositoryBean
public abstract interface TrackRepository<T extends EntityTrack> extends BaseRepository<T> {

    /**
     * Collect a custom view to resume data of parameters.
     * @param devicesId {@link device#id}
     * @param interval {@link IntervalType}
     * @param startAt {@link Date}
     * @param endAt {@link Date}
     * @return List of {@link NodeProjection}
     */
    @Query(
            value = " SELECT date_trunc(:interval, e.created_at\\:\\:timestamp) AS date," +
                    "    count(*) AS count," +
                    "    AVG(e.value) AS average," +
                    "    MAX(e.value) AS maximum," +
                    "    MIN(e.value) AS minimum" +
                    " FROM app_param_#{#entityName} e" +
                    "   INNER JOIN sys_device d ON e.device_id = d.id" +
                    " WHERE e.created_at >= :startAt AND e.created_at <= :endAt" +
                    "   AND d.id IN :devicesId" +
                    " GROUP BY 1" +
                    " ORDER BY 1",
            nativeQuery = true
            )
    public List<NodeProjection> getHourly (
            @Param("devicesId") final List<Long> devicesId,
            @Param("interval") final String interval,
            @Param("startAt") final Date startAt,
            @Param("endAt") final Date endAt
            );

    @Query(value =  "SELECT DISTINCT d.id"
                    + " FROM #{#entityName} e"
                    + "     INNER JOIN e.device d")
    public List<Long> getAllDevicesIdByParameter();
}
