/* **********************************************************************
 * PowerRepository.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.repository.parameter;
import com.angot.domotique.entity.parameter.Power;

public interface PowerRepository extends TrackRepository<Power> {

}
