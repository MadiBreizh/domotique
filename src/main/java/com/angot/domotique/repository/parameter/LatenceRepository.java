/* **********************************************************************
 * LatenceRepository.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.repository.parameter;

import java.util.List;

import com.angot.domotique.entity.parameter.Latence;

public interface LatenceRepository extends TrackRepository<Latence> {

    /**
     * return entities with specific zone name.
     * @param hostname
     * @return
     */
    List<Latence> findAllByZoneName(String zoneName);

}
