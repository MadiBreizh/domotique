/* **********************************************************************
 * BaseRepository.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import com.angot.domotique.entity.EntityBase;

@NoRepositoryBean
public abstract interface BaseRepository<T extends EntityBase> extends JpaRepository<T, Long> {

    /**
     * Return entity with specific id.
     * @param id
     * @return
     */
    @Query(value = "SELECT e"
            + " FROM #{#entityName} e"
            + " WHERE e.id = :id")
    Optional<T> retrieveOne(@Param("id") Long id);


    /**
     * Return all entities.
     * @return
     */
    @Query(value = "SELECT e"
            + " FROM #{#entityName} e")
    Optional<List<T>> retrieveAll();

    /**
     * Return all active entities.
     * @return
     */
    @Query(value = "SELECT e"
            + " FROM #{#entityName} e"
            + " WHERE e.disableTag = false" )
    Optional<List<T>> retrieveAllActive();
}
