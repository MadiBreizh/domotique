/* **********************************************************************
 * ZoneRepository.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.repository;

import java.util.Optional;

import com.angot.domotique.entity.Device;
import com.angot.domotique.entity.Zone;

/** Repository use for {@link Zone} entity. */
public interface ZoneRepository extends BaseRepository<Zone> {

    /**
    * Return {@link Optional} of entity {@link Device} from given name {@link Zone#name}.
    * @param zoneName String
    * @return {@link Optional} of {@link Zone}
    */
    Optional<Zone> findByName(String zoneName);
}
