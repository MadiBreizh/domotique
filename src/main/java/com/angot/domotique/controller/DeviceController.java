/* **********************************************************************
 * DeviceController.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.angot.domotique.dto.DeviceDTO;
import com.angot.domotique.dto.RestDTO;
import com.angot.domotique.entity.Device;
import com.angot.domotique.entity.Zone;
import com.angot.domotique.exception.BadRequestException;
import com.angot.domotique.exception.NotFoundException;
import com.angot.domotique.mapper.DeviceMapper;
import com.angot.domotique.repository.DeviceRepository;
import com.angot.domotique.repository.ZoneRepository;

import lombok.AccessLevel;
import lombok.Getter;

@RestController
@RequestMapping(value = DeviceController.ROUTE_ROOT)
@Getter(value = AccessLevel.PRIVATE)
public class DeviceController extends BaseController<Device, DeviceRepository>  {

    static final String ROUTE_ROOT = BaseController.ROUTE_API + "/device";

    private static final String ROUTE_ASSIGN_ZONE = BaseController.ROUTE_GET_ID + "/link";

    static final String MSG_IDENTIFIER_ALREADY_USE = "Device identifier is already use.";

    /** Use to map {@link Device device} with DTO. */
    @Autowired
    private DeviceMapper deviceMapper;

    /** Use to find {@link Zone zone} and assign this one with a {@link Device device}.*/
    @Autowired
    private ZoneRepository zoneRepository;

    /**
    * Creates an entity {@link Device} from given {@link DeviceDTO dto}.
    * @param dto {@link DeviceDTO}
    * @return
    */
    @PostMapping()
    public ResponseEntity<DeviceDTO> create(@RequestBody @Valid DeviceDTO dto){
        return new ResponseEntity<>(this.getDeviceMapper().to(this.createOne(new Device(), dto)), HttpStatus.OK);
    }

    /**
    * Updates an {@link Device device} from given {@link DeviceDTO dto}.
    * @param id Long
    * @param dto {@link DeviceDTO}
    * @return
    */
    @PutMapping(BaseController.ROUTE_GET_ID)
    public ResponseEntity<Device> update(@PathVariable Long id, @RequestBody @Valid DeviceDTO dto) {
        return new ResponseEntity<>(this.updateOne(id, dto), HttpStatus.OK);
    }

    /**
     * Assigns {@link Zone zone} for {@link Device device}.
     * @param id
     * @param dto DeviceDTO
     * @return
     */
    @PutMapping(DeviceController.ROUTE_ASSIGN_ZONE)
    public ResponseEntity<DeviceDTO> assignZone(@PathVariable Long id, @RequestBody DeviceDTO dto) {
        Device device = this.retrieveOne(id);

        // Remove old Zone.
        if (dto.getZoneId() == null) {
            // Check if device has zone to be remove.
            device.getZone().removeDevice(device);
        } else {
            // Control and add new Zone.
            this.getZoneRepository().retrieveOne(dto.getZoneId())
                    .map(zone -> zone.addDevice(device))
                    .orElseThrow(() -> new NotFoundException("Zone", dto.getZoneId()));
        }

        return new ResponseEntity<>(this.getDeviceMapper().to(this.getRepository().save(device)), HttpStatus.OK);
    }

    /**
     * Return one {@link Device device}.
     * @param id
     * @return
     */
    @GetMapping(BaseController.ROUTE_GET_ID)
    public ResponseEntity<?> getOne(@PathVariable Long id) {
        return new ResponseEntity<>(this.getDeviceMapper().to(this.getRepository().findOne(id)), HttpStatus.OK);
    }

    /**
     * Return all active entities with {@link Device#disableTag} = False.
     * @return
     */
    @GetMapping()
    public ResponseEntity<List<DeviceDTO>> getAllActive() {
        return new ResponseEntity<>(this.getDeviceMapper().to(this.findAllActive()), HttpStatus.OK);
    }

    /**
     * Return all entities {@link Device}.
     * @return
     */
    @GetMapping(BaseController.ROUTE_GET_ALL)
    public ResponseEntity<List<DeviceDTO>> getAll() {
        return new ResponseEntity<>(this.getDeviceMapper().to(this.findAll()), HttpStatus.OK);
    }

    /**
     * Disable one entity {@link Device} from given id.
     * @param id
     * @return
     */
    @DeleteMapping(BaseController.ROUTE_GET_ID)
    public ResponseEntity<Device> desable(@PathVariable Long id) {
        // TODO: Remove zone before disable.
        return new ResponseEntity<>(this.disableOne(id), HttpStatus.OK);
    }

    /**
     * (non-Javadoc)
     * @see com.angot.domotique.controller.BaseController#fromDto
     */
    @Override
    protected Device fromDto(final Device entity, final RestDTO dto) {
        DeviceDTO deviceDto = (DeviceDTO) dto;

        if (this.isNew(entity)) {
            // TODO: Check if exist device with disableTag true to reuse it.
            this.getRepository().findByIdentifier(deviceDto.getIdentifier())
                    .ifPresent(device -> {
                        throw new BadRequestException(DeviceController.MSG_IDENTIFIER_ALREADY_USE);
                    });
        }

        this.getDeviceMapper().from(deviceDto, entity);

        if (deviceDto.getZoneId() != null) {
            this.getZoneRepository().retrieveOne(deviceDto.getZoneId())
                    .map(zone -> zone.addDevice(entity))
                    .orElseThrow(() -> new NotFoundException("Zone", deviceDto.getZoneId()));
        }

        return entity;
    }
}

