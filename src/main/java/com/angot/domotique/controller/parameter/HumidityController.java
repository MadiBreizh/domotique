/* **********************************************************************
 * HumidityController.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.controller.parameter;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.angot.domotique.controller.BaseController;
import com.angot.domotique.controller.TrackController;
import com.angot.domotique.entity.parameter.Humidity;
import com.angot.domotique.repository.parameter.HumidityRepository;

@RestController
@RequestMapping(value = HumidityController.ROUTE_ROOT)
public class HumidityController extends TrackController<Humidity, HumidityRepository> {

    static final String ROUTE_ROOT = BaseController.ROUTE_API + "/humidity";

}
