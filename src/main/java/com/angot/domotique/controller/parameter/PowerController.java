/* **********************************************************************
 * PowerController.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.controller.parameter;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.angot.domotique.controller.BaseController;
import com.angot.domotique.controller.TrackController;
import com.angot.domotique.entity.parameter.Power;
import com.angot.domotique.repository.parameter.PowerRepository;

@RestController
@RequestMapping(value = PowerController.ROUTE_ROOT)
public class PowerController extends TrackController<Power, PowerRepository> {

    static final String ROUTE_ROOT = BaseController.ROUTE_API + "/power";

}
