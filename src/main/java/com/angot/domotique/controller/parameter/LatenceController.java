/* **********************************************************************
 * LatenceController.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.controller.parameter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.angot.domotique.controller.BaseController;
import com.angot.domotique.entity.parameter.Latence;
import com.angot.domotique.mapper.LatenceMapper;
import com.angot.domotique.repository.parameter.LatenceRepository;

import lombok.AccessLevel;
import lombok.Getter;

@RestController
@RequestMapping(value = LatenceController.ROUTE_ROOT)
@Getter(value = AccessLevel.PRIVATE)
public class LatenceController extends BaseController<Latence, LatenceRepository> {

    static final String ROUTE_ROOT = BaseController.ROUTE_API + "/latence";

    /** Service use to map dtos with entities. */
    @Autowired
    private LatenceMapper latenceMapper;


}
