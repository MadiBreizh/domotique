/* **********************************************************************
 * Rfxtrx433Controller.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.angot.domotique.dto.receiver.RfxcomDTO;
import com.angot.domotique.service.RfxcomService;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.log4j.Log4j;

@RestController
@RequestMapping(value =  Rfxtrx433Controller.ROUTE_ROOT)
@Getter(value = AccessLevel.PRIVATE)
@Log4j
public class Rfxtrx433Controller {

    static final String ROUTE_ROOT = BaseController.ROUTE_API + "/rfxtrx433";

    @Autowired
    private RfxcomService rfxcomService;

    /**
     * Uses to receive raw data and store this one.
     * @param dto
     */
    @PostMapping
    public void retrieveData(@RequestBody final RfxcomDTO dto) {
        Rfxtrx433Controller.log.info("SENSOR | Data incoming from : " + dto.getTopic());

        this.getRfxcomService().storeData(dto);
    }

}
