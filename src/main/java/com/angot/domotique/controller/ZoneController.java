/* **********************************************************************
 * ZoneController.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.angot.domotique.dto.RestDTO;
import com.angot.domotique.dto.ZoneDTO;
import com.angot.domotique.entity.Zone;
import com.angot.domotique.exception.ConflictException;
import com.angot.domotique.mapper.ZoneMapper;
import com.angot.domotique.repository.ZoneRepository;

import lombok.AccessLevel;
import lombok.Getter;

@RestController
@RequestMapping(value = ZoneController.ROUTE_ROOT)
@Getter(value = AccessLevel.PRIVATE)
public class ZoneController extends BaseController<Zone, ZoneRepository> {

	static final String ROUTE_ROOT = BaseController.ROUTE_API + "/zone";

	
    /** Uses to map {@link Zone } with DTO. */
    @Autowired
    private ZoneMapper zoneMapper;

    /**
    * Creates an entity {@link Zone} from given dto {@link ZoneDTO}.
    * @param dto {@link ZoneDTO}
    * @return
    */
    @PostMapping()
    public ResponseEntity<?> create(@RequestBody @Valid ZoneDTO dto){
        return new ResponseEntity<>(this.createOne(new Zone(), dto), HttpStatus.OK);
    }

    /**
    * Updates an entity {@link Zone} from given dto {@link ZoneDTO}.
    * @param id Long
    * @param dto {@link ZoneDTO}
    * @return
    */
    @PutMapping(ROUTE_GET_ID)
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody @Valid ZoneDTO dto) {
        return new ResponseEntity<>(this.updateOne(id, dto), HttpStatus.OK);
    }

    /**
     * Returns one entity {@link Zone}.
     * @param id
     * @return
     */
    @GetMapping(ROUTE_GET_ID)
    public ResponseEntity<?> getOne(@PathVariable Long id) {
        return new ResponseEntity<>(this.getZoneMapper().to(this.retrieveOne(id)), HttpStatus.OK);
    }

    /**
     * Returns all active entities with {@link Zone#disableTag} = False.
     * @return
     */
    @GetMapping()
    public ResponseEntity<?> getAllActive() {
        return new ResponseEntity<>(this.getZoneMapper().to(this.findAllActive()), HttpStatus.OK);
    }

    /**
     * Returns all entities {@link Zone}.
     * @return
     */
    @GetMapping(ROUTE_GET_ALL)
    public ResponseEntity<List<ZoneDTO>> getAll() {
        return new ResponseEntity<>(this.getZoneMapper().to(this.findAll()), HttpStatus.OK);
    }

    /**
     * Disables one entity {@link Zone} from given id.
     * @param id
     * @return
     */
    @DeleteMapping(ROUTE_GET_ID)
    public ResponseEntity<?> delete(@PathVariable Long id) {
        Zone entity = this.retrieveOne(id);

        entity.getDevices().stream().map(e -> e).collect(Collectors.toList())
                .forEach(device -> entity.removeDevice(device));

        this.getRepository().save(this.disableOne(id));

        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @Override
    protected Zone fromDto(final Zone entity, final RestDTO dto) {
        ZoneDTO zoneDto = (ZoneDTO) dto;

        if (entity.getId() == null) {
            this.getRepository().findByName(zoneDto.getName().trim())
            .map(found -> {
                throw new ConflictException(found.getName());
                });
        }

        this.getZoneMapper().from(zoneDto, entity);

        return entity;
    }
}
