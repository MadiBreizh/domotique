/* **********************************************************************
 * InfoController.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 14, 2019
 *
 * ************************************************************************/

package com.angot.domotique.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.angot.domotique.entity.Zone;
import com.angot.domotique.service.RfxcomService;
import com.angot.domotique.service.ZoneService;

import lombok.AccessLevel;
import lombok.Getter;

@RestController
@RequestMapping(value = InfoController.ROUTE_ROOT)
@Getter(AccessLevel.PRIVATE)
public class InfoController {

    static final String ROUTE_ROOT = BaseController.ROUTE_API + "/info";

    /** The {@link Zone zone} services.*/
    @Autowired
    private ZoneService zoneService;
    
    
    /** The {@link Zone Device} services.*/
    @Autowired
    private RfxcomService deviceService;
    

    /**
     * Returns  a summary of information available.
     * @return
     */
    @GetMapping
    public Map<String, Object> getInfo() {
        final Map<String, Object> results = new HashMap<String, Object>();
        
        List<Zone> zones = this.getZoneService().getAll();
        results.put("nbr_zones", zones.size());
        results.put("zones", zones);
        
        return results;
    }
}
