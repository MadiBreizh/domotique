/* 
 * **********************************************************************
 * AbstractController.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import com.angot.domotique.dto.RestDTO;
import com.angot.domotique.entity.EntityBase;
import com.angot.domotique.exception.NoContentException;
import com.angot.domotique.exception.NotFoundException;
import com.angot.domotique.repository.BaseRepository;

import lombok.AccessLevel;
import lombok.Getter;

@Getter(value = AccessLevel.PROTECTED)
public abstract class BaseController<T extends EntityBase, R extends BaseRepository<T>> {

	protected static final String REG_GET_NUMBER = "^\\d+$";

	protected static final String ROUTE_GET_ID = "/{id:" + REG_GET_NUMBER + "}";
	
	protected static final String ROUTE_GET_ALL = "/full";

    protected static final String ROUTE_API = "/api/v1";

    /** Repository. */
    @Autowired
    private R repository;

    /**
     * Helper to known if entity is new.
     * @param entity T
     * @return
     */
    protected boolean isNew(T entity) {
        return entity.getId() == null;
    }

    /**
     * Returns all entities.
     * @return
     */
    protected List<T> findAll() {
        return this.getRepository().retrieveAll()
                .map(e -> e)
                .orElseThrow(() -> new NoContentException());
    }

    /**
    * Returns all active entities.
    * @return
    */
   protected List<T> findAllActive() {
       return this.getRepository().retrieveAllActive()
               .map(e -> e)
               .orElseThrow(() -> new NoContentException());
   }

    /**
    * Disable entity for given id.
    * @param id Long
    * @return
    */
   protected T disableOne(final Long id) {
       T entity = this.retrieveOne(id);
       return this.disableOne(entity);
   }

   /**
   * Disable entity for given entity.
   * @param id T
   * @return
   */
    protected T disableOne(final T entity) {
        // set status disable for this entity.
        entity.setDisableTag(true);
        return this.getRepository().save(entity);
    }

    /**
     * Update one entity with given id.
     * @param id Long
     * @param dto {@link RestDTO}
     * @return
     */
    protected T updateOne(final Long id, final RestDTO dto) {
        return this.createOne(this.retrieveOne(id), dto);
    }

    /**
     * Returns entity for given id.
     * @param id
     * @return
     */
    protected T retrieveOne(final Long id) {
        return this.getRepository().retrieveOne(id)
                .map(e -> e)
                .orElseThrow(() -> new NotFoundException(id));
    }

    /**
     * Creates new entity.
     * @param entity
     * @param dto
     * @return
     */
    protected T createOne(final T entity, final RestDTO dto) {
        this.fromDto(entity, dto);
        return this.getRepository().save(entity);
    }

    /**
     *
     * @param entity
     * @param dto
     * @return
     */
    protected T fromDto(final T entity, final RestDTO dto) {
        return entity;
    }
}

