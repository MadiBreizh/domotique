/* **********************************************************************
 * TrackController.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 07, 2019
 *
 * ************************************************************************/

package com.angot.domotique.controller;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.angot.domotique.entity.EntityTrack;
import com.angot.domotique.entity.IntervalType;
import com.angot.domotique.entity.parameter.Node;
import com.angot.domotique.exception.NoContentException;
import com.angot.domotique.repository.parameter.TrackRepository;

/** Controller for {@link EntityTrack} entities. */
public abstract class TrackController<T extends EntityTrack, R extends TrackRepository<T>> extends BaseController<T, R> {

//    /**
//     * Get all {@link EntityTrack entity}.
//     * @return
//     */
//    @GetMapping()
//    protected List<T> findAllEntities() {
//        return this.getRepository().findAll();
//    }

    /**
     * Collect a summary of information for a given period.
     * @param startAt {@link ZonedDateTime}
     * @param endAt {@link ZonedDateTime}
     * @param devicesId {@link Long}
     * @param period {@link IntervalType}
     * @return
     */
    @GetMapping()
    public List<Node> retrieveAverage(
            @RequestParam(value = "startAt",    required = true)
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)     final ZonedDateTime startAt,

            @RequestParam(value = "endAt",      required = true)
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)     final ZonedDateTime endAt,

            @RequestParam(value = "deviceId",   required = false)   List<Long> devicesId,
            @RequestParam(value = "interval",   required = true)     final IntervalType interval
            ) {
        if (devicesId == null) {
            devicesId = this.getRepository().getAllDevicesIdByParameter();

            if (devicesId.isEmpty()) {
                throw new NoContentException();
            }
        }

        return this.getRepository().getHourly(
                devicesId,
                interval.toString(),
                Date.from(startAt.toInstant()),
                Date.from(endAt.toInstant()))
                .stream()
                .map(entity -> new Node(
                        ZonedDateTime.ofInstant(entity.getDate().toInstant(), ZoneId.systemDefault()),
                        entity.getCount(),
                        entity.getAverage(),
                        entity.getMinimum(),
                        entity.getMaximum()))
                .collect(Collectors.toList());
    }
}
