/* **********************************************************************
 * DeviceDTO.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.dto;

import org.hibernate.validator.constraints.NotBlank;

import com.angot.domotique.entity.Device;

import lombok.Data;
import lombok.experimental.Accessors;

/** Data transfert object use to represent a {@link Device} entity */
@Data
@Accessors(chain = true)
public class DeviceDTO implements RestDTO {

    @NotBlank
    private String name;

    @NotBlank
    private String identifier;

    private Long zoneId;
}
