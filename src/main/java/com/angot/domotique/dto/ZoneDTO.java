/* **********************************************************************
 * ZoneDTO.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.dto;

import java.util.List;

import org.hibernate.validator.constraints.NotBlank;

import com.angot.domotique.entity.Zone;
import com.angot.domotique.entity.ZoneType;

import lombok.Data;
import lombok.experimental.Accessors;

/** Data transfert object use to represent a {@link Zone} entity */
@Data
@Accessors(chain = true)
public class ZoneDTO implements RestDTO {

    private Long id;

    private boolean disableTag;

    /**  */
    @NotBlank
    private String name;

    private ZoneType type;

    private List<DeviceDTO> devices;

}
