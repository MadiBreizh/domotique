/* **********************************************************************
 * ReceiverDTO.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 08, 2019
 *
 * ************************************************************************/

package com.angot.domotique.dto.receiver;

import com.angot.domotique.dto.RestDTO;
import com.angot.domotique.dto.parameter.ParameterDTO;
import com.angot.domotique.dto.parameter.StatusDTO;

import lombok.Getter;
import lombok.experimental.Accessors;

@Getter
@Accessors(chain = true)
public abstract class ReceiverDTO implements RestDTO {

    private StatusDTO status;

    private ParameterDTO temperature;

    private ParameterDTO humidity;

    private ParameterDTO power;

    private ParameterDTO energy;

}
