/* **********************************************************************
 * DataRfxcomDTO.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.dto.receiver;

import com.angot.domotique.dto.parameter.StatusDTO;

import lombok.Getter;
import lombok.experimental.Accessors;

@Getter
@Accessors(chain = true)
public class RfxcomDTO extends ReceiverDTO {

    private String topic;

    private StatusDTO status;
    
    protected String type;

}
