/* **********************************************************************
 * ParameterDTO.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 09, 2019
 *
 * ************************************************************************/

package com.angot.domotique.dto.parameter;

import lombok.Getter;
import lombok.experimental.Accessors;

@Getter
@Accessors(chain = true)

public class ParameterDTO {

    /** Value. */
    protected Double value;

}
