/* **********************************************************************
 * LatenceDTO.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.dto.parameter;

import java.time.ZonedDateTime;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.angot.domotique.converter.DatabaseDateConverter;
import com.angot.domotique.dto.RestDTO;
import com.angot.domotique.entity.parameter.Latence;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

import lombok.Data;
import lombok.experimental.Accessors;

/** Data transfer object use to represent a {@link Latence} entity */
@Data
@Accessors(chain = true)
public class LatenceDTO implements RestDTO {
    @NotNull
    @NotBlank
    private String hostname;

    @NotNull
    private Double latence;

    @JsonFormat(shape = Shape.STRING, pattern = DatabaseDateConverter.FORMAT)
    private ZonedDateTime createdAt;
}
