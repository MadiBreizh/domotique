/* **********************************************************************
 * Node.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 29, 2019
 *
 * ************************************************************************/

package com.angot.domotique.entity.parameter;

import java.util.Date;

/** Project to represent a custom view for parameters. */
public interface NodeProjection {
    Date getDate();
    Integer getCount();
    Double getAverage();
    Double getMaximum();
    Double getMinimum();
}
