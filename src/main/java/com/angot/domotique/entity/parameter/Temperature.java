/* **********************************************************************
 * Temperature.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.entity.parameter;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.angot.domotique.contract.parameter.TemperatureContract;
import com.angot.domotique.entity.Device;
import com.angot.domotique.entity.EntityTrack;
import com.angot.domotique.entity.Zone;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Accessors(chain = true)
@Entity
@Table(name = TemperatureContract.TABLE_NAME)
public class Temperature extends EntityTrack {

    /** Constructor. */
    public Temperature(final Double value, final Zone zone, final Device device) {
        this.zone = zone;
        this.device = device;
        this.value = value;
    }
}
