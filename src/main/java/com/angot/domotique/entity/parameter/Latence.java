/* **********************************************************************
 * Latence.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.entity.parameter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.angot.domotique.contract.parameter.LatenceContract;
import com.angot.domotique.entity.EntityTrack;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/** Entity represent latence. */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = LatenceContract.TABLE_NAME)
public class Latence extends EntityTrack {
}
