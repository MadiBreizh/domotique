/* **********************************************************************
 * Node.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 29, 2019
 *
 * ************************************************************************/

package com.angot.domotique.entity.parameter;

import java.time.ZonedDateTime;

import com.angot.domotique.entity.EntityBase;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

import lombok.AllArgsConstructor;
import lombok.Data;

/** Represents some data of parameters for one interval. */
@Data
@AllArgsConstructor
public class Node {

    /** Date of interval. */
    @JsonFormat(shape = Shape.STRING, pattern = EntityBase.FORMAT_DATE)
    private ZonedDateTime date;

    /** Number of values for this node. */
    private Integer count;

    /** Average of values. */
    private double average;

    /** Value minimal. */
    private double minimum;

    /** Value maximal. */
    private double maximum;

}
