package com.angot.domotique.entity;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;

import org.springframework.data.annotation.CreatedDate;

import com.angot.domotique.contract.EntityBaseContract;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@MappedSuperclass
public abstract class EntityBase {

    public static final String FORMAT_DATE = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZ";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = EntityBaseContract.COL_ID)
    private Long id;

    /** Use to disable {@link Device} and {@link Zone}. */
    @Column(name = EntityBaseContract.COL_DISABLE_TAG)
    private boolean disableTag;

    @JsonFormat(shape = Shape.STRING, pattern = EntityBase.FORMAT_DATE)
    @CreatedDate
    @Column(name = EntityBaseContract.COL_CREATEDAT)
    private ZonedDateTime createdAt;

    @PrePersist
    public void prePersist() {
        this.createdAt = ZonedDateTime.now();
    }
}
