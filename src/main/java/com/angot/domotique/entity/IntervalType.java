/* **********************************************************************
 * IntervalType.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Feb 01, 2019
 *
 * ************************************************************************/

package com.angot.domotique.entity;

/** Defines each interval available. */
public enum IntervalType {
    /** Each hours.. */
    HOUR(IntervalType.VALUES.HOUR),
    /** Each days. */
    DAY(IntervalType.VALUES.DAY),
    /** Each weeks. */
    WEEK(IntervalType.VALUES.WEEK),
    /** Each months. */
    MONTH(IntervalType.VALUES.MONTH),
    /** Each quarter. (trimestre) */
    QUARTER(IntervalType.VALUES.QUARTER),
    /** Each years. */
    YEAR(IntervalType.VALUES.YEAR);

    public static class VALUES {
        public static final String HOUR = "hour";
        public static final String DAY = "day";
        public static final String WEEK = "week";
        public static final String MONTH = "month";
        public static final String QUARTER = "quarter";
        public static final String YEAR = "year";
    }


    /** The name field. */
    private final String value;

    private IntervalType(String value) {
        this.value = value;
    }

    public String getValue() { return this.value; }
}
