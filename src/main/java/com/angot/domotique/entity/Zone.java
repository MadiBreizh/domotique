/* **********************************************************************
 * Zone.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.angot.domotique.contract.ZoneContract;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = ZoneContract.TABLE_NAME)
public class Zone extends EntityBase {

    /** Name field. */
    @Column(name = ZoneContract.COL_NAME, unique=true)
    private String name;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = ZoneContract.COL_TYPE)
    private ZoneType type = ZoneType.REAL;

    @OneToMany(mappedBy = "zone", cascade=CascadeType.PERSIST)
    @JsonIgnore
    private List<Device> devices = new ArrayList<>();

    /** Constructor */
    public Zone(final String name, final ZoneType type) {
        super();
        this.name = name;
        this.type = type;
    }

    /**
     * Add {@link Device} with specified {@link Zone}.
     * @param device Device
     * @return Zone the modified site
     */
    public Zone addDevice(Device device) {
        if (!this.devices.contains(device)) {
            this.devices.add(device);

            if (device.getZone() != this) {
                device.setZone(this);
            }
        }

        return this;
    }

    /**
     * Remove {@link Device} with specified {@link Zone}.
     * @param device Device
     * @return Zone the modified site
     */
    public Zone removeDevice(Device device) {
        if (this.devices.contains(device)) {
            this.devices.remove(device);

            if (device.getZone() == this) {
                device.setZone(null);
            }
        }

        return this;
    }
}
