/* **********************************************************************
 * Device.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.entity;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.angot.domotique.contract.DeviceContract;
import com.angot.domotique.contract.ZoneContract;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/** The {@link Device} entity. */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Entity
@Table(name = DeviceContract.TABLE_NAME)
public class Device extends EntityBase {

    /** The name field. */
    @NotNull
    @Column(name = DeviceContract.COL_NAME)
    private String name;
    
    /** The last sync date field. */
    @Column(name = DeviceContract.COL_SYNC_DATE)
    private ZonedDateTime sync_date;

    /** The identifier field. */
    @NotNull
    @Column(name = DeviceContract.COL_IDENTIFIER, unique=true)
    private String identifier;

    /** The battery level field. */
    @Column(name = DeviceContract.COL_LVL_BATTERY)
    private Long battery;

    /** The reception level field. */
    @Column(name = DeviceContract.COL_LVL_RECEPTION)
    private Long reception;

    @ManyToOne()
    @JoinColumn(name = ZoneContract.COL_ZONE_ID)
    private Zone zone;

    /** Constructor */
    public Device(final String name, final String identifier) {
        super();
        this.name = name;
        this.identifier = identifier;
    }
}
