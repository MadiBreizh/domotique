/* **********************************************************************
 * ZoneType.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.entity;

import java.lang.reflect.Field;

public enum ZoneType {
    REAL(0),
    VIRTUAL(1);

    /** The name field. */
    private final int value;

    private ZoneType(int value) {
        this.value = value;
        try {
            final Field field = this.getClass().getSuperclass().getDeclaredField("ordinal");
            field.setAccessible(true);
            field.set(this, this.value);
       } catch (Exception e) {//or use more multicatch if you use JDK 1.7+
            throw new RuntimeException(e);
      }
    }

    public int getValue() { return this.value; }
}
