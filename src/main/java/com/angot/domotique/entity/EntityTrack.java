/* **********************************************************************
 * EntityTrack.java, domotique
 *
 * Copyright 2019 Erwan Angot
 * Description :
 * Author(s)   : Erwan Angot <erwan@malangot.fr>
 * License     : all right reserved
 * Created at  : Jan 06, 2019
 *
 * ************************************************************************/

package com.angot.domotique.entity;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import com.angot.domotique.contract.DeviceContract;
import com.angot.domotique.contract.EntityTrackContract;
import com.angot.domotique.contract.ZoneContract;
import com.angot.domotique.contract.parameter.EnergyContract;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@MappedSuperclass
public abstract class EntityTrack extends EntityBase {
    
    /** Field value. */
    @NotNull
    @Column(name = EntityTrackContract.COL_VALUE)
    protected Double value;
    
    /**  */
    @ManyToOne
    @NotNull
    @JoinColumn(name = ZoneContract.COL_ZONE_ID)
    @JsonIgnore
    protected Zone zone;

    /**  */
    @ManyToOne
    @NotNull
    @JoinColumn(name = DeviceContract.COL_DEVICE_ID)
    @JsonIgnore
    protected Device device;
}
